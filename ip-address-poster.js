'use strict';
// https://www.npmjs.com/package/telnet-client
const telnet = require('telnet-client');
const info = require('.\\creds.js');

async function run () {
	let session = await new telnet();

	let params = {
		host: info.gatewayIp,
		password: info.password,
		port: 23,
		shellPrompt: ' #'
	}

	session.connect(params)
		.then( function (prompt) {

		})


}